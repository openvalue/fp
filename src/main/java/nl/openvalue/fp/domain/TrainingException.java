package nl.openvalue.fp.domain;

public class TrainingException extends Exception {
    public TrainingException(String message) {
        super(message);
    }
}
