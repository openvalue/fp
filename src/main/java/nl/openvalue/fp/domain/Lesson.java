package nl.openvalue.fp.domain;

public class Lesson {
    private String subject;

    public Lesson(String subject) {
        this.subject = subject;
    }

    public String subject() {
        return subject;
    }

    public void subject(String subject) {
        this.subject = subject;
    }
}
