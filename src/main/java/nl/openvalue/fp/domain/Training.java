package nl.openvalue.fp.domain;

import java.util.List;

public class Training {
    private Integer id;
    private String name;
    private Trainer trainer;
    private List<Lesson> lessons;

    public Training(Integer id, String name, Trainer trainer, final List<Lesson> lessons) {
        this.id = id;
        this.name = name;
        this.trainer = trainer;
        this.lessons = lessons;
    }

    public Integer id() {
        return id;
    }

    public void id(Integer id) {
        this.id = id;
    }

    public String name() {
        return name;
    }

    public void name(String name) {
        this.name = name;
    }

    public Trainer trainer() {
        return trainer;
    }

    public void trainer(Trainer trainer) {
        this.trainer = trainer;
    }

    public List<Lesson> lessons() {
        return lessons;
    }

    public void lessons(List<Lesson> lessons) {
        this.lessons = lessons;
    }

    @Override
    public String toString() {
        return "Training{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", trainer=" + trainer +
                ", lessons=" + lessons +
                '}';
    }
}
