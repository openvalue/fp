package nl.openvalue.fp.best_practices;

import nl.openvalue.fp.domain.Trainer;
import nl.openvalue.fp.domain.Training;
import nl.openvalue.fp.domain.TrainingException;

import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

public class Monads {

    final static Map<Integer, Training> inMemoryDatabase = Map.of(
            1, new Training(1, "Functional Programming", new Trainer(1, "Klaas"), List.of()),
            2, new Training(2, "Object-oriented Programming", new Trainer(1, "Klaas"), List.of())
    );

    public static void main(String[] args) {
        var training = getTrainingById(4);
        System.out.println(training);

        try {
            var anotherTraining = getTrainingByName("Functional Programming");
            System.out.println(anotherTraining);
        } catch (TrainingException e) {
            System.out.println("Could not find training!!");
        }
    }

    // todo this method can return null, fix it by using a Optional or Result monad
    private static Training getTrainingById(final int id) {
        return inMemoryDatabase.get(id);
    }

    // todo this method can throw exceptions, refactor it into functional style
    // todo hint: if you want, and are able to, use vavr's Option or Try:
    // todo see: https://docs.vavr.io/#_option
    // todo hint: the signature of the method then becomes: private static Option<Training> getTrainingByName(String name)
    private static Training getTrainingByName(final String name) throws TrainingException {
        if ("Object-oriented Programming".equals(name)) {
            throw new TrainingException("We're not doing object-oriented!");
        }
        for (final Map.Entry<Integer, Training> entry : inMemoryDatabase.entrySet()) {
            if (name.equals(entry.getValue().name())) {
                return entry.getValue();
            }
        }
        throw new TrainingException("No training found");
    }
}
