package nl.openvalue.fp.immutability;

import nl.openvalue.fp.domain.Lesson;
import nl.openvalue.fp.domain.Trainer;
import nl.openvalue.fp.domain.Training;

import java.util.List;

public class Immutable {

    public static void main(String[] args) {
        var lesson = new Lesson("Immutable data structures");
        var trainer = new Trainer(1, "Stijn");
        var training = new Training(1, "Functional programming", trainer, List.of(lesson));

        lesson.subject("Mutable data structures");
        training.trainer(new Trainer(1, "Jan"));

        // todo make the domain model in nl.openvalue.fp.example immutable

        // todo once you have done that, try to make this code work, with keeping immutability
//         training.addLesson(new Lesson("pure functions"));
    }
}
