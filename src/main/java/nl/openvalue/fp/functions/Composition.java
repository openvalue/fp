package nl.openvalue.fp.functions;

import nl.openvalue.fp.domain.Lesson;
import nl.openvalue.fp.domain.Trainer;
import nl.openvalue.fp.domain.Training;

import java.util.List;

public class Composition {


    public static void main(String[] args) {
        var training = new Training(1, "Functional Programming", new Trainer(1, "Hans"), List.of(
                new Lesson("Pure functions"),
                new Lesson("Higher-order functions"),
                new Lesson("Side-effects")
        ));
    }

    // todo make some functions that do something with a training, for example transform the name
    // and compose them
}
