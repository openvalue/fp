package nl.openvalue.fp.functions;

import nl.openvalue.fp.domain.Lesson;
import nl.openvalue.fp.domain.Trainer;
import nl.openvalue.fp.domain.Training;

import java.util.List;
import java.util.Random;

public class PureFunctions {

    public static void main(String[] args) {
        var training = createTraining("Functional programming");

        printTraining(training);

        updateTrainingWithLesson(training, new Lesson("No side effects"));

        // todo make these functions pure, no side-effects!
    }

    static Training createTraining(String name) {
        // todo how do you handle this side-effect?
        var randomId = new Random().nextInt();

        return new Training(randomId, name, createTrainer(randomId, "Joop"),
                List.of(new Lesson("How to avoid side effects")));
    }

    static Trainer createTrainer(Integer id, String name) {
        return new Trainer(id, name);
    }

    static void printTraining(Training training) {
        training.id(20);
        System.out.println(training);
    }

    static void updateTrainingWithLesson(Training training, Lesson lesson) {
        System.out.printf("Adding lesson %s to training%n", lesson);
        training.lessons().add(lesson);
    }

}
