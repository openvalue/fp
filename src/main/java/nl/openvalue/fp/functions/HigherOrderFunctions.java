package nl.openvalue.fp.functions;

import nl.openvalue.fp.domain.Lesson;
import nl.openvalue.fp.domain.Trainer;
import nl.openvalue.fp.domain.Training;

import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

public class HigherOrderFunctions {

    public static void main(String[] args) {
        // todo add a `Duration duration` and `boolean mandatory` to the Lesson domain class,
        //  indicating how long each lesson takes and if a lesson is mandatory.
        var training = new Training(1, "Functional Programming", new Trainer(1, "Hans"), List.of(
                new Lesson("Pure functions"),
                new Lesson("Higher-order functions"),
                new Lesson("Side-effects")
        ));


        // todo implement the following functions, with higher order functions in mind
    }

    // hint: stream the lessons, mapping the duration to an int, and use the sum()-method
    static int getTotalDuration(Training training) {
        return -1;
    }

    // hint: use the filter higher-order function to use the Predicate<Lesson>
    // the Predicate<Lesson> is just a Function<Lesson, Boolean> which always has to return a boolean
    static List<Lesson> getMandatoryLessons(Training training, Predicate<Lesson> isMandatory) {
        return List.of();
    }

    // hint: use the .map()-method
    static List<String> getLessonNames(Training training, Function<Lesson, String> lessonName) {
        return List.of();
    }
}
