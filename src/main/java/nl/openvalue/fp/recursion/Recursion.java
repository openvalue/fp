package nl.openvalue.fp.recursion;

import nl.openvalue.fp.domain.Lesson;
import nl.openvalue.fp.domain.Trainer;
import nl.openvalue.fp.domain.Training;

import java.util.List;

public class Recursion {

    public static void main(String[] args) {
        var training = new Training(1, "Functional Programming", new Trainer(1, "Hans"), List.of(
                new Lesson("Pure functions"),
                new Lesson("Higher-order functions"),
                new Lesson("Side-effects")
        ));

        var totalDuration = getTotalDuration(training);

        System.out.printf("This training takes %d", totalDuration);
    }

    // todo implement this method using recursion
    static int getTotalDuration(Training training) {
        return -1;
    }
}
