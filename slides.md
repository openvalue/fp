---
title: Functional programming
revealOptions:
  transition: 'fade'
  progress: 'c/t'
---

# Functional programming


Stijn Vet

2023-03-23

---

<section>

# Introduction

<i class="arrow down"></i>
</section>
<section>

## Agenda

![img.png](img.png)

</section>
<section>

## Practical

- Take a break if you need one
- There's drinks and snacks in the kitchen
- Ask questions straight away!
- Each part of the training is a mix of theory and programming (by you!)
- Be in control of your learning experience

</section>
<section>

## Me

- Stijn
- 35 years old 
- DevOps engineer at OpenValue
- Kotlin/Java/Spring/SRE
- FP enthousiast
</section>
<section>

## You

- Programming experience?
- Functional experience?
- What do you expect?

</section>

---

<section>

# Introduction to functional programming

<i class="arrow down"></i>
</section>
<section>

## What is functional programming?

Five pillars of functional programming:
- Immutability
- Pure functions
- Higher-order functions
- Recursion
- Function composition


<aside class="notes">
Today, we'll be talking about five core concepts of functional programming. Immutability, pure functions, high-order functions, recursion and composition.
Functional programming is a programming paradigm that focuses on functions to solve problems. 
It is based on the idea of treating computation as the evaluation of mathematical functions.
It therefore has a really strong connection to mathematics and has a lot of concepts translated from maths into programming.




A pure function is one that has no side effects and always returns the same output given the same input. This makes it easier to reason about the behavior of a program and reduces the likelihood of bugs caused by unexpected state changes.

Functions are first-class citizens and can be passed as arguments to other functions, returned as values, or stored in data structures. This allows for powerful abstractions and composability of functions.

Recursion is often used instead of loops in functional programming to perform repetitive tasks. It can be more expressive and lead to clearer code, but can also have performance implications in some cases.

In functional programming, functions can be composed to create more complex functions from simpler ones. This can lead to code that is more modular and easier to understand and maintain.
</aside>
</section>

<section>

## Intro: Immutability 

- Data classes cannot be modified after construction
- Easier to reason about code
- Prevents unintended side-effects
- Simplifies concurrency

<aside class="notes">
Immutability refers to the concept of creating data structures that cannot be modified after they are created.

This means that once a value is assigned to a variable, it cannot be changed. 

Immutability is a fundamental concept in functional programming because it allows for referential transparency.

Referential transparency is the concept that a function can be replaced by its return value, without changing the
behavior of the program. This helps with composing functions.

</aside>
</section>
<section>

## Intro: Immutability - disadvantages

- Can be less efficient than mutable data structures
- Can require more memory allocation and garbage collection

</section>
<section>

## Intro: Pure functions 

- Always return the same result for a given input
- No side-effects
  - No modification of external state 
  - Only depend on input
- Enables referential transparency and memoization
- Simplifies concurrency

<aside class="notes">
Memoization is a technique to speed up the execution of functions by caching results of functions. Pure functions
make this possible because they guarantee that there are no side-effects.
</aside>
</section>
<section>

## Intro: Pure functions - disadvantages

- Oftentimes we need side-effects
  - Like logging, saving to a database
- Copying data might make impure functions more efficient

</section>

<section>

## Intro: Higher-order functions

- Functions that take other functions as arguments
- Functions that return a function
- Create powerful abstractions
- Reuse code
- More expressive and declarative
</section>
<section>

## Intro: Higher-order functions - disadvantages

- Can be more difficult to reason about
</section>
<section>

## Intro: Recursion

- Function calling itself
- No loops in functional programming
- Simplifying what to do with each element
- Can be more efficient
</section>
<section>

## Intro: Recursion - disadvantages

- Can be hard
- StackOverflowError / performance issues
</section>
<section>

## Intro: Function composition

- Stitch together functions to solve the problem
- More declarative and each smaller function is easier to understand
- Each smaller function does a specific thing

</section>
<section>

## Intro: Function composition - disadvantages

- Can be more difficult to reason about 
- Can lead to inefficient code

</section>
---

<section>

## Concept: Immutability

</section>
<section>

### Immutability
- Focus on immutable data structures
  - Once created, it cannot be modified
  - No risk of conflicting modification
  - No risk of changing state

```java
class Greeting { // in java17+: record
  public final String text;

  Greeting(String text) {
    this.text = text;
  }
}
```

```java
class Example {
  public static void main(String[] args) {
    var greeting = new Greeting("hello!");
    
    greeting.text = "No bueno";
    System.out.println(greeting.text);
  }
}
```

<aside class="notes">
as we can see in the example, we can only create a new Greeting by a constructor, there's no setter.
</aside>
</section>
<section>

### Exercise

- Clone the repo
  - `git clone https://gitlab.com/openvalue/fp.git` 
- Open in your favourite Java IDE (it's Java 11)
- Fix the todos in the `nl.openvalue.fp.immutability.Immutable`-class, while keeping the domain classes immutable.

</section>

---

<section>

## Concept: Pure functions

</section>
<section>

### How can we define a function?


</section>
<section>

### A function is a programming concept that
  - Takes some input
  - Computes/transforms that input
  - Gives back output
- Logically group statements together to improve readability


</section>

<section>

### What about pure functions?

- No side effects
- Deterministic output
- The function purely depends on the input and has no effects on the program or the environment

```java
class Example {
    public Integer addTwo(final Integer number) {
        return number + 2;
    }
}
```

</section>

<section>

### Is this a pure function? Why?

```java
class Example {
    private final DatabaseService service;

    public Integer addTwo(final Integer number) {
        var result = number + 2;
        service.save(result);
        return result;
    }
}
```

</section>
<section>

### Why is this important?

- Predictability
- Testability
- Reusability
- Parallelism

<aside class="notes">
Each call to a pure function, will always give back the same result. 
That makes it predictable and easier to reason about.

No side effects mean easier to test

Reusable since they only do what they tell you to do

You can call pure functions in parallel without having to worry about concurrency conflicts.
</aside>
</section>

<section>

### Exercise

- Fix the todos in `nl.openvalue.fp.functions.PureFunctions`
</section>

---

<section>

## Concept: Higher-order functions


</section>
<section>

### Higher-order function

- A higher-order function is just a function
- It can take a function as an argument or return a function

</section>
<section>

```java
default <V> Function<T, V> andThen(
        Function<? super R, ? extends V> after
) {
      Objects.requireNonNull(after);
      return (T t) -> after.apply(apply(t));
  }
```

```java
class HigherOrderExample {
    public void example() {
      Function<Integer, Integer> addTwo = x -> x + 2;
      Function<Integer, Integer> multiplyByTwo = x -> x * 2;

      var result = addTwo.andThen(multiplyByTwo).apply(2);
    }
}

```

<small> This example also shows function composition which we will see later</small>
</section>
<section>

### Why is this important?

- Create more expressive and reusable code
  - For instance the function `map`
  - Generic function that you provide another function to transform your collection

</section>
<section>

```java
class HigherOrderExample {
    public void objectOriented() {
        List<Integer> list = new ArrayList<>();

        for (final Integer integer : List.of(1, 2)) {
            var subResult = addTwo.apply(integer);
            var finalResult = multiplyByTwo.apply(subResult);
            list.add(finalResult);
        }

        System.out.println(list);
    }
}
```

```java
class HigherOrderExample {
  public void functionalOriented() {
    var list = Stream.of(1, 2)
            .map(addTwo)
            .map(multiplyByTwo)
            .toList();

    System.out.println(list);
  }
}

```

</section>
<section>

### Exercise

- Fix the todos in `nl.openvalue.fp.functions.HigherOrderFunctions`
</section>
---

<section>

## Concept: Recursion


</section>
<section>

### Recursion
- Expressive and elegant way to solve problems
- A function calling itself
- No traditional loops in functional programming
- Way to iterate through data
- Break up your big problem into smaller sub problems

</section>
<section>

### The standard recursion example: factorial

```java
public class Recursion {
    public static int factorial(int n) {
        if (n == 0) {
            return 1;
        }
        int result = 1;
        for (int i = 1; i <= n; i++) {
            result *= i;
        }
        return result;
    }

    public static void main(String[] args) {
        int result = factorial(5);
        System.out.println(result);
    }
}

```
</section>
<section>

### In functional style
```java
public class Recursion {
    public static int factorial(int n) {
        if (n == 0) {
            return 1;
        }
        return factorial(n);
    }

    public static void main(String[] args) {
        int result = factorial(5);
        System.out.println(result);
    }
}

```
</section>
<section>

### Exercise

- Fix the todos in `nl.openvalue.fp.recursion.Recursion`
</section>

---

<section>

## Concept: Function composition

</section>
<section>

### Function composition
- Technique to combine two or more functions in a single function
- Higher-order functions make this possible
- Create a chain of functions that solve your problem, one step at the time
</section>
<section>

### Function composition

```java
class Example {
    Function<String, String> addPrefix = x -> "prefix-" + x;
    Function<String, String> addPostfix = x -> x + "-postfix";
    Function<String, String> addParenthesis = x -> "(" + x + ")";
    
    public void compose() {
        var formattedString = addPrefix
                .andThen(addPostfix)
                .andThen(addParenthesis)
                .apply("1");

      System.out.println(formattedString);
    }
}
```
</section>
<section>

### Exercise

- Fix the todos in `nl.openvalue.fp.functions.Composition`


</section></section></section>

---

## Best practices

<section>

- Avoid mutable state
- Side-effects only if absolutely necessary
- Pure functions!
  - Much easier to test
  - Makes refactoring easier
- Following these three principles almost erases the need for tests
  - If it compiles, it will probably work!



</section>
<section>

- Avoid nulls by using the type system
- Avoid exceptions by using the type system
  - Use a concept like `Result` or `Optionals` (Monads)
- Use that type system to do pattern matching
  - Requires Java17 (or a real functional language)
- Kotlin has a much richer standard library for FP
- Use a library like vavr.io or Arrow for Kotlin

</section>
